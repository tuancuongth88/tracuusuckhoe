

import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';

class TabToolsScreen extends StatefulWidget with GetItStatefulWidgetMixin{
  @override
  State<StatefulWidget> createState() => _StateTabToolsScreen();

}
class _StateTabToolsScreen extends State<TabToolsScreen> with GetItStateMixin{
  var tabIndex = 0;

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return DefaultTabController(
      initialIndex: tabIndex,
      length: 2,
      child: Scaffold(
          backgroundColor: Colors.blue,
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0),
            child: AppBar(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15),),
              ),
              backgroundColor: Colors.white,
              automaticallyImplyLeading: false,
              bottom: TabBar(
                tabs: [
                  Tab(child: Text('Thông tin', style: text_action_font16)),
                  Tab(child: Text('Số liệu vắc xin', style: text_action_font16)),
                ],
              ),

            ),
          ),
          body: TabBarView(
            children: [
              ToolsItemScreen(tabIndex: 0,),
              ToolsItemScreen(tabIndex: 1,),
            ],
          )

      ),
    );
  }

}


class ToolsItemScreen extends StatefulWidget with GetItStatefulWidgetMixin{
  final int tabIndex;

  ToolsItemScreen({Key? key, required this.tabIndex}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _StateToolsItemScreen();

}
class _StateToolsItemScreen extends State<ToolsItemScreen> with GetItStateMixin{

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    final orientation = MediaQuery.of(context).orientation;
    return Container(
      color: Colors.white,
      child: GridView.builder(
        gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: (orientation == Orientation.portrait) ? 2 : 3,
            crossAxisSpacing: 8,
            mainAxisSpacing: 8),
        padding: EdgeInsets.all(8),
        itemCount: 10,
        itemBuilder: (BuildContext context, int index) {
          return Container(
              decoration: BoxDecoration(
                border: Border.all(width: 1, color: Colors.black12),
                borderRadius: BorderRadius.all(Radius.circular(5)), ),
              child: InkWell(
                onTap: () {
                  //đi đên trang chi tiết
                },
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      height: MediaQuery.of(context).size.height * 0.15,
                      child: ClipRRect(
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(5), topRight: Radius.circular(5)),
                        child: Image.network("https://www.iigvietnam.com/modules/mod_btslideshow/images/91/slideshow/ssat-banner-web.jpg",
                          fit: BoxFit.cover,

                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(5),
                      // width: ,
                      child: Text("TOEIC IPT", style: text_fontsize12_black,),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 5, left: 5),
                      child: Row(
                        children: [
                          Text("Giá:", style: text_fontsize12_black,),
                          SizedBox(width: 10,),
                          Text("1.500.000 đ", style: text_fontsize12_black,)
                        ],
                      ),
                    ),
                    Container(
                      margin: const EdgeInsets.only(left: 5, right: 5, top: 5),
                      padding: const EdgeInsets.only(left: 10, right: 10, top: 2, bottom: 2),
                      decoration: BoxDecoration(
                        border: Border.all(width: 1, color: txtColorAction),
                        borderRadius: BorderRadius.all(Radius.circular(15)),
                        color: colorPink.withOpacity(0.3),
                      ),
                      child: Text("Thêm vào giỏ", style: text_fontsize12_black,),
                    )

                  ],
                ),
              ));
        },
      ),
    );
  }

}