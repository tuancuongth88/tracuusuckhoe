import 'package:flutter/material.dart';
import 'package:get_it_mixin/get_it_mixin.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';
import 'package:tracuusuckhoe/screen/view/tab/tab_tools_screen.dart';
import 'package:tracuusuckhoe/screen/widget/marquee_widget.dart';


class MenuPage extends StatefulWidget with GetItStatefulWidgetMixin {
  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> with GetItStateMixin {
  GlobalKey<ScaffoldState> _key = GlobalKey();
  // var controller = GetIt.I<MenuHomeController>();
  // var controllerUserInfo = GetIt.I<UserController>();
  String title = "Trang chủ";

  @override
  void initState() {
    // TODO: implement initStffate
    super.initState();
    // controllerUserInfo.userInfo();
    // controller.getMenuHome();
  }

  @override
  Widget build(BuildContext context) {

    // final userInfo = watchX((MenuHomeModel x) => x.userInfo);

    return WillPopScope(
      onWillPop: _onBackPressed,
      child: Scaffold(
        key: _key,
        appBar: AppBar(
          actionsIconTheme: IconThemeData(color: Theme.of(context).primaryColorLight),
          iconTheme: IconThemeData(color: Theme.of(context).primaryColorLight),
          backgroundColor: Colors.transparent,
          flexibleSpace: Container(
            decoration: BoxDecoration(image: DecorationImage(
                image : Image.asset("assets/images/ic_bg_default.jpg"
                ).image, fit: BoxFit.cover)
            ),
          ),
          leading: Theme(
            data: ThemeData(
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
            ),
            child: InkWell(
              onTap: () {
                // GetIt.I<UserController>().userInfo();
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(
                //       builder: (context) => ThayDoiThongTinScreen()),
                // );
              },
              child: Icon(Icons.menu,
                size: 35,
                color: Theme.of(context).primaryColorLight,),
              // child: Public.iconCustom(
              //   icon: user,
              //   context: context,
              //   size: icon_70sp,
              //   color: Theme.of(context).primaryColorLight,
              // )
            ),
          ),
          elevation: 0,
          title: title == ''
              ? MarqueeWidget(
            child: Text( "",
              style: text_fontsize20_white,
            ),
          )
              : MarqueeWidget(
            child: Text(title, style: text_fontsize20_white),
          ),
          centerTitle: true,
          actions: <Widget>[

          ],
        ),
        // appBar: CustomAppBar(context),
        // drawer: AppDrawer(),
        body: callPage(currentIndex),
        bottomNavigationBar: BottomNavBar(),
      ),
    );
  }

  Future<bool> _onBackPressed() {
    // DialogAlert.logOut(context);
    return Future.value(true);
  }

  Widget callPage(int page) {
    switch (page) {
      case 0:
        return TabToolsScreen();
      case 1:
        return TabToolsScreen();
      case 2:
        return TabToolsScreen();
      case 3:
        return TabToolsScreen();
      case 4:
        return TabToolsScreen();
      default:
        return TabToolsScreen();
    }
  }

  int currentIndex = 0;

  setBottomBarIndex(index) {
    setState(() {
      switch(index){
        case 0:
          title = "Trang chủ";
          break;
        case 1:
          title = "Đăng ký tiêm";
          break;
        case 2:
          title = "Tra cứu chứng nhận tiêm";
          break;
        case 3:
          title = "Tra cứu kết quả";
          break;
      }
      currentIndex = index;

    });
  }

  Widget BottomNavBar() {
    final Size size = MediaQuery.of(context).size;
    final bool ip = size.height >= 812.0;
    final double btpadding = ip ? 20.0 : 0.0;
    final double height = ip ? 70 : 60;
    return Container(
      color: colorPrimary,
      // color: colorEAF0F8,
      width: size.width,
      // padding: EdgeInsets.only(bottom: btpadding),
      height: height,
      child: Stack(
        children: [
         /* Center(
            child: FloatingActionButton(
                backgroundColor: Colors.white,
                child: Icon(
                  Icons.qr_code_scanner,
                  color: colorPrimary,
                ),
                elevation: 0.1,
                onPressed: () {
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (context) =>
                              QRscanScreen(QRDetailScreen())));
                }),
          ),*/
          Container(
            width: size.width,
            height: height,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                IconButton(
                  icon: Icon(
                    Icons.home,
                    size: 28,
                    color: currentIndex == 0
                        ? Color(0xFF0F7EF1)
                        : Colors.grey.shade200,
                  ),
                  onPressed: () {
                    setBottomBarIndex(0);
                  },
                  splashColor: Colors.white,
                ),
                IconButton(
                    icon: Icon(
                      Icons.school_outlined,
                      size: 28,
                      color: currentIndex == 1
                          ? Color(0xFF0F7EF1)
                          : Colors.grey.shade200,
                    ),
                    onPressed: () {
                      setBottomBarIndex(1);
                    }),
                IconButton(
                    icon: Icon(
                      Icons.article_outlined,
                      size: 28,
                      color: currentIndex == 2
                          ? Color(0xFF0F7EF1)
                          : Colors.grey.shade200,
                    ),
                    onPressed: () {
                      setBottomBarIndex(2);
                    }),
                IconButton(
                    icon: Icon(
                      Icons.article_outlined,
                      size: 28,
                      color: currentIndex == 3
                          ? Color(0xFF0F7EF1)
                          : Colors.grey.shade200,
                    ),
                    onPressed: () {
                      setBottomBarIndex(3);
                    }),
              ],
            ),
          )
        ],
      ),
    );
  }
}
