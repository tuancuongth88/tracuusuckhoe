import 'package:flutter/material.dart';

class CardWidget extends StatelessWidget {
  final List<Widget> children;
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Color shadowColor ;
  const CardWidget({required this.children, required this.margin, required this.padding, required this.shadowColor});

  @override
  Widget build(BuildContext context) {
    return Card(
      color: Colors.transparent,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(16))),
      shadowColor: shadowColor ?? Colors.transparent,
      elevation: 0.1,
      margin: margin,
      child: Padding(
        padding: padding == null?EdgeInsets.all(24):padding,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.max,
          children: children,
        ),
      ),
    );
  }
}
