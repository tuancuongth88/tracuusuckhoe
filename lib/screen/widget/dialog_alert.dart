import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';



typedef ValueOtp<String> = String Function(String);

class DialogAlert {
  // ignore: inference_failure_on_function_return_type
  static showMLoadding(BuildContext context) {
    // ignore: inference_failure_on_function_return_type
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Container(
            color: Colors.transparent,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(colorPrimary),
                ),
              ],
            ),
          );
        });
  }

  // ignore: inference_failure_on_function_return_type
  static dissmissLoadingDialog(BuildContext context, GlobalKey key) {
    Navigator.of(context).pop();
  }


  static logOut(BuildContext context) {
    // ignore: inference_failure_on_function_return_type
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
            // backgroundColor: Colors.blue,
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)), //this right here
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 20),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "logOut.title",
                          textAlign: TextAlign.center,
                          style: TextStyle(fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        Text(
                          "logOut.ok" + " !",
                          textAlign: TextAlign.center,
                        ),
                        SizedBox(
                          height: 40,
                        ),
                      ]),
                ),
                Container(
                  decoration: BoxDecoration(
                    border: Border(
                      top: BorderSide(color: Theme.of(context).primaryColor),
                    ),
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      // ignore: avoid_unnecessary_containers
                      Container(
                        child: Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                right: BorderSide(
                                    color: Theme.of(context).primaryColor),
                              ),
                            ),
                            child: FlatButton(
                                // color: blueAccent,
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                child: Text(
                                  'logOut.cancel',
                                  style: TextStyle(
                                      fontSize: 18,
                                      color: Theme.of(context).primaryColor),
                                )),
                          ),
                        ),
                      ),
                      Flexible(
                        flex: 1,
                        fit: FlexFit.tight,
                        child: FlatButton(
                            onPressed: () {
                              // menu cũng phai update lại về là menu ngoài login
                              // var controller = GetIt.I<UserController>();
                              // controller.logOut();
                              // SharePreferUtils.logOutApp();
                              // // đồng ý thoát app xóa các trang cũ đi
                              // Phoenix.rebirth(context);

                            },
                            child: Text('logOut.yes',
                                style: TextStyle(
                                    fontSize: 18,
                                    color: Theme.of(context).primaryColor))),
                      ),
                    ],
                  ),
                )
              ],
            ),
          );
        });
  }

  static void showDialogAlertCancel(BuildContext context, String content) {
    showDialog(
        barrierDismissible: false,
        context: context,
        builder: (context) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20)), //this right here
            child: SizedBox(
              height: MediaQuery.of(context).size.height * 0.3,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  SizedBox(
                    height: 20,
                  ),
                  Text('Thông báo',
                      style: TextStyle(
                          fontSize: 50, fontWeight: FontWeight.bold)),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    margin: EdgeInsets.symmetric(horizontal: 100),
                    child: Center(
                      child: Text(content),
                    ),
                  ),
                  SizedBox(
                    height: 30,
                  ),
                  Container(
                    height: 120,
                    decoration: BoxDecoration(
                      border: Border(
                        top: BorderSide(color: Colors.grey),
                      ),
                    ),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Flexible(
                          flex: 1,
                          fit: FlexFit.tight,
                          child: FlatButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text('Đồng ý',
                                  style: TextStyle(
                                      fontSize: 50,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).primaryColor))),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
          );
        });
  }

}
