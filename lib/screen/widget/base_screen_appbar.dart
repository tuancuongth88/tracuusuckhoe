

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:keyboard_dismisser/keyboard_dismisser.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';

class BaseAppbarScreen extends StatelessWidget{
  final String title;
  final bool isShowIcon;
  final List<Widget> children;

  BaseAppbarScreen({required this.title, required this.isShowIcon, required this.children});

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
        body: Container(
          child: KeyboardDismisser(
            child: Stack(
              children: [
                Container(
                  decoration: BoxDecoration(image: DecorationImage(image : Image.asset(
                      "assets/images/ic_bg_default.jpg"
                  ).image,colorFilter: ColorFilter.mode(Colors.white, BlendMode.multiply),fit: BoxFit.cover)),),
                Container(
                  margin: const EdgeInsets.only(top: 50, left: 16, right: 16),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      isShowIcon ? InkWell(

                        child: Icon(Icons.arrow_back, color: Colors.white,),
                        onTap: () => Navigator.of(context).pop(false),
                      ): SizedBox(),
                      Expanded(
                        child: Text(title, style: text_button_white, textAlign: TextAlign.center,),
                      ),

                    ],
                  ),
                ),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height ,
                    margin: const EdgeInsets.only(top: 100),
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(topLeft: Radius.circular(15), topRight: Radius.circular(15))),
                    child: Column(
                      children: children,

                    ),
                  ),
                ),
              ],
            ),
          ),
        ));
  }

}