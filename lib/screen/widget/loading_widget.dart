

import 'package:flutter/material.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';

class LoadingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: colorWhite.withOpacity(0.5),
      child: Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(Colors.blue.shade800),
        ),
      ),
    );
  }
}