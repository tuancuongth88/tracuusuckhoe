import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tracuusuckhoe/screen/style/style.dart';

class BtnChoosePopup extends StatefulWidget {
  final Function onTab;
  final String title;
  final bool check;
  BtnChoosePopup({required this.onTab, required this.title, required this.check});

  @override
  _BtnChoosePopupState createState() => _BtnChoosePopupState();
}

class _BtnChoosePopupState extends State<BtnChoosePopup> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: InkWell(
          onTap: () => widget.onTab,
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                border: Border(bottom: BorderSide(color: Colors.blue[50]!))),
            child: Padding(
              padding: EdgeInsets.only(left: 12, right: 12),
              child:
                Container(
                  height: 40,
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      widget.title,
                      style: text_fontsize12_black,
                    ),
                  ),
                ),
            ),
          )),
    );
  }
}
