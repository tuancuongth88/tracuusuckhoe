import 'dart:html';

import 'package:dio/dio.dart';
import 'package:flutter/foundation.dart';
import 'package:retrofit/retrofit.dart';

part 'api_services.g.dart';

@RestApi()
abstract class ApiServices {
  factory ApiServices(Dio dio, {String? baseUrl}) = _ApiServices;
  String? baseUrl;

  @POST("api/userInfo")
  Future<AuthenticatorResponse> userInfo();

  @POST("public/dashboard/vaccination-statistics/summary")
  Future<AuthenticatorResponse> summary();
  @POST("public/dashboard/vaccination-location/search")
  Future<AuthenticatorResponse> search();
  @GET("vaccination/public/otp-search")
  Future<AuthenticatorResponse> otpSearch({
    @Field() required String fullName,
    @Field() required String birthday,
    @Field() required String genderId,
    @Field() required String personalPhoneNumber,
    @Field() required String identification,
    @Field() required String healthInsuranceNumber,});

}
